import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { AuthenticationService } from './authentication.service';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

  constructor(private authenticationService: AuthenticationService) {}

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if (this.authenticationService.isAuthorized()) {
      const authRequest = request.clone({ setHeaders: { 'Authorization': `Bearer ${this.authenticationService.getAccessToken()}`}});
      return next.handle(authRequest);
    } else {
      next.handle(request);
    }
  }

}
