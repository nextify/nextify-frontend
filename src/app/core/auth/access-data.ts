// Samplce access data

export interface AccessData {
	access_token: string;
	refresh_token: string;
  expires_in: number;
	token_type: string;
}
