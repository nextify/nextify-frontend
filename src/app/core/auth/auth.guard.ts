import { CanLoad, CanActivate, Router, Route, ActivatedRouteSnapshot } from '@angular/router';
import { Injectable } from '@angular/core';
import { AuthenticationService } from './authentication.service';

@Injectable()
export class AuthGuard implements CanLoad, CanActivate {

constructor(private authenticationService: AuthenticationService, private router: Router) {}

  canLoad(route: Route): boolean {
    const authorized = this.authenticationService.isAuthorized();
    if (!authorized) {
      this.handleLogin();
    }
    return authorized;
  }

  canActivate(route: ActivatedRouteSnapshot): boolean {
    const authorized = this.authenticationService.isAuthorized();
    if (!authorized) {
      this.handleLogin();
    }
    return authorized;
  }

  handleLogin(path?: string) {
    this.router.navigate(['/login']);
  }
}
