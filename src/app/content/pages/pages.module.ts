import { LayoutModule } from '../layout/layout.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PagesRoutingModule } from './pages-routing.module';
import { PagesComponent } from './pages.component';
import { PartialsModule } from '../partials/partials.module';
import { ProfileComponent } from './header/profile/profile.component';
import { CoreModule } from '../../core/core.module';
import { FormsModule } from '@angular/forms';
import { ErrorPageComponent } from './snippets/error-page/error-page.component';
import { InnerComponent } from './components/inner/inner.component';
import {
	MatButtonModule,
	MatFormFieldModule,
	MatInputModule,
	MatCheckboxModule
} from '@angular/material';
import { AuthGuard } from '../../core/auth/auth.guard';
import { PagesCoreModule } from './pages.core.module';

@NgModule({
	declarations: [
    PagesComponent,
		ProfileComponent,
		ErrorPageComponent,
		InnerComponent
	],
	imports: [
		CommonModule,
    FormsModule,
		PagesRoutingModule,
		CoreModule,
    PagesCoreModule,
		LayoutModule,
		PartialsModule,
		MatButtonModule,
		MatFormFieldModule,
		MatInputModule,
    MatCheckboxModule
	],
	providers: [
    AuthGuard
  ]
})
export class PagesModule {}
