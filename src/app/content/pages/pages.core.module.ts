import { NgModule } from '@angular/core';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { getPaginatorIntl } from '../../../app/config/i18n/pt-BR.paginator';
import { MatPaginatorIntl} from '@angular/material';
// Shared
import { AuthInterceptor } from '../../core/auth/auth.interceptor';
@NgModule({
  imports: [HttpClientModule],
  exports: [HttpClientModule],
  providers: [
    { provide: MatPaginatorIntl, useValue: getPaginatorIntl() },
    { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true },
  ]
})
export class PagesCoreModule {}
