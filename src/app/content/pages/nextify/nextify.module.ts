import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { NextifyComponent } from './nextify.component';
import { NextifyRoutingModule } from './nextify-routing.module';
import { ActionComponent } from '../header/action/action.component';
import { AngularEditorModule } from '@kolkov/angular-editor';
import { AuthGuard } from '../../../core/auth/auth.guard';

@NgModule({
  declarations: [NextifyComponent, ActionComponent],
  imports: [
    CommonModule,
    FormsModule,
    NextifyRoutingModule,
    AngularEditorModule
  ],
  providers: [
    AuthGuard
  ]
})
export class NextifyModule {}
