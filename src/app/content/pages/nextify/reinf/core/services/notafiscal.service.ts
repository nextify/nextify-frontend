import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, from, forkJoin, BehaviorSubject, of } from 'rxjs';
import { ReinfNotaFiscalServicoModel } from '../models/ReinfNotaFiscalServico.model';
import { QueryParamsModel } from '../../../core/models/query-models/query-params.model';
import { QueryResultsModel } from '../../../core/models/query-models/query-results.model';
import { HttpUtilsService } from '../../../../../../core/services/http-utils.service';
import { mergeMap, filter, map, tap } from 'rxjs/operators';
import { ReinfNotaFiscalServicoItemModel } from '../models/ReinfNotaFiscalServicoItem.model';
import { ListStateModel, StateActions } from '../utils/list-state.model';

const API_NOTAFISCAL_URL = 'http://localhost:3000/v1/reinf/notafiscal';


@Injectable()
export class ReinfNotaFiscalService {
  constructor(private http: HttpClient, private httpUtils: HttpUtilsService) {}

  create(product): Observable<ReinfNotaFiscalServicoModel> {
    return this.http.post<ReinfNotaFiscalServicoModel>(API_NOTAFISCAL_URL, product);
  }

  update(notaFiscal: ReinfNotaFiscalServicoModel): Observable<any> {
    // Note: Add headers if needed (tokens/bearer)
    const url = API_NOTAFISCAL_URL + `/${notaFiscal._id}`;
    const httpHeaders = this.httpUtils.getHTTPHeaders();
    return this.http.put(url, notaFiscal, { headers: httpHeaders });
  }

  createTipoServico(tiposervico: ReinfNotaFiscalServicoItemModel, lastState: ListStateModel): Observable<any> {
    const url = `${API_NOTAFISCAL_URL}/tiposervico`;
    const _tiposervico = new ReinfNotaFiscalServicoItemModel();

    Object.assign(_tiposervico, tiposervico);

    return this.http.post(url, _tiposervico.getModel(), { observe: 'response' }).pipe(
      tap(response => {
        if (response.ok && response.status === 201) {
          lastState.removeItem(tiposervico, StateActions.CREATE);
        }
      })
    );
  }

  updateTipoServico(tiposervico: ReinfNotaFiscalServicoItemModel, lastState: ListStateModel): Observable<any> {
    const url = `${API_NOTAFISCAL_URL}/tiposervico/${tiposervico._id}`;
    const _tiposervico = new ReinfNotaFiscalServicoItemModel();

    Object.assign(_tiposervico, tiposervico);

    return this.http.put(url, _tiposervico.getModel(), { observe: 'response' }).pipe(
      tap(response => {
        if (response.ok && response.status === 200) {
          lastState.removeItem(tiposervico, StateActions.UPDATE);
        }
      })
    );
  }

  deleteTipoServico(tiposervico: ReinfNotaFiscalServicoItemModel, lastState: ListStateModel): Observable<any> {
    const url = `${API_NOTAFISCAL_URL}/tiposervico/${tiposervico._id}`;

    return this.http.delete(url, { observe: 'response'}).pipe(
      tap(response => {
        if (response.ok && response.status === 200) {
          lastState.removeItem(tiposervico, StateActions.DELETE);
        }
      })
    );
  }


  getAll(): Observable<ReinfNotaFiscalServicoModel[]> {
    return this.http.get<ReinfNotaFiscalServicoModel[]>(API_NOTAFISCAL_URL);
  }

  listItems(lastState: ListStateModel): Observable<QueryResultsModel> {
    const url = `${API_NOTAFISCAL_URL}/${lastState.entityId}/items`;
    const pendingItems = of(lastState.pendingItems());
    const pendingItemsId = lastState.pendingItemsId();
    console.log(lastState);
    const result = this.http.post<ReinfNotaFiscalServicoItemModel[]>(url, { pending: pendingItemsId });

    return forkJoin(result, pendingItems).pipe(
      map(([array1, array2]) => new QueryResultsModel(array1, array2))
    );
  }

  getById(_id: string): Observable<ReinfNotaFiscalServicoModel> {
    return this.http.get<ReinfNotaFiscalServicoModel>(API_NOTAFISCAL_URL + `/${_id}`);
  }

  findNotaFiscal(queryParams: QueryParamsModel): Observable<QueryResultsModel> {
    const httpHeaders = this.httpUtils.getHTTPHeaders();
    const httpParams = this.httpUtils.getFindHTTPParams(queryParams);

    const url = API_NOTAFISCAL_URL + '/find';
    return this.http.get<QueryResultsModel>(url, {
      headers: httpHeaders,
      params: httpParams
    });
  }
}
