import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, from, forkJoin, BehaviorSubject, of } from 'rxjs';
import { HttpUtilsService } from '../../../../../../core/services/http-utils.service';

import { ApiConfig } from '../../../../../../config/api';
import { ReinfPeriodoApuracaoAnoMesModel, ReinfPeriodoApuracaoModel } from '../models/reinf-periodoapuracao.model';
import { QueryResultsModel } from '../../../core/models/query-models/query-results.model';
import { QueryParamsModel } from '../../../core/models/query-models/query-params.model';

const API_URL = `${ApiConfig.url}/reinf/periodoapuracao`;

@Injectable()
export class ReinfPeriodoApuracaoService {
  lastFilter$: BehaviorSubject<QueryParamsModel> = new BehaviorSubject(new QueryParamsModel({}, 'desc', '', 0, 10));

  constructor(private http: HttpClient, private httpUtils: HttpUtilsService) {}

  create(periodo: ReinfPeriodoApuracaoAnoMesModel): Observable<ReinfPeriodoApuracaoModel> {
    return this.http.post<ReinfPeriodoApuracaoModel>(API_URL, periodo);
  }

  list(): Observable<ReinfPeriodoApuracaoModel[]> {
    return this.http.get<ReinfPeriodoApuracaoModel[]>(API_URL);
  }

  find(queryParams: QueryParamsModel): Observable<QueryResultsModel> {
    const httpHeaders = this.httpUtils.getHTTPHeaders();
    const httpParams = this.httpUtils.getFindHTTPParams(queryParams);

    return this.http.get<QueryResultsModel>(API_URL, {
      headers: httpHeaders,
      params: httpParams
    });
  }
}
