import { BaseModel } from '../../../core/models/base.model';

export class ReinfNotaFiscalServicoItemINSS {
  aliquota: number;
  vlrBaseRetencao: number;
  vlrRetencao: number;
  vlrRetSub: number;
  vlrNRetPrinc: number;
  vlrServicos15: number;
  vlrServicos20: number;
  vlrServicos25: number;
  vlrAdicional: number;
  vlrNRetAdic: number;
}

export class ReinfNotaFiscalServicoItemModel extends BaseModel {
  _id: string;
  notafiscal: string;
  tipoServico: string;
  inss: ReinfNotaFiscalServicoItemINSS;

  clear(notaFiscalId: string) {
    this.notafiscal = notaFiscalId;
    this.tipoServico = '';
    this.inss = new ReinfNotaFiscalServicoItemINSS();
    this.inss.aliquota = 0;
    this.inss.vlrBaseRetencao = 0;
    this.inss.vlrRetencao = 0;
  }

  getModel () {
    return {
      _id: this._id,
      notafiscal: this.notafiscal,
      tipoServico: this.tipoServico,
      inss: {
        aliquota: this.inss.aliquota,
        vlrBaseRetencao: this.inss.vlrBaseRetencao,
        vlrRetencao: this.inss.vlrRetencao,
        vlrRetSub: this.inss.vlrRetSub,
        vlrNRetPrinc: this.inss.vlrNRetPrinc,
        vlrServicos15: this.inss.vlrServicos15,
        vlrServicos20: this.inss.vlrServicos20,
        vlrServicos25: this.inss.vlrServicos25,
        vlrAdicional: this.inss.vlrAdicional,
        vlrNRetAdic: this.inss.vlrNRetAdic
      }
    };
  }
}
