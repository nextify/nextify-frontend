import { PessoaModel } from '../../../core/models/pessoa.model';

export class ReinfNotaFiscalServicoModel {
  _id: string;
  num: number;
  serie: string;
  dataEmissao: string;
  pessoa: PessoaModel;
  tipoOperacao: string;
  vlrBruto: number;
  observacao: string;
}
