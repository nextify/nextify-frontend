import { of } from 'rxjs';
import { catchError, finalize, tap } from 'rxjs/operators';
import { QueryResultsModel } from '../../../../core/models/query-models/query-results.model';
import { BaseDataSource } from '../../../../core/models/datasources/base.datasource';
import { ReinfNotaFiscalService } from '../../services/notafiscal.service';
import { QueryParamsModel } from '../../../../core/models/query-models/query-params.model';
import { ListStateModel } from '../../utils/list-state.model';

export class ReinfNotaFiscalTipoServicoDataSource extends BaseDataSource {
  constructor(private notafiscalService: ReinfNotaFiscalService) {
    super();
  }

  loadAll(queryParams: QueryParamsModel, lastState: ListStateModel) {
    // this.notafiscalService.lastFilter$.next(queryParams);
    this.loadingSubject.next(true);
    // console.log(lastState);
    this.notafiscalService
      .listItems(lastState)
      .pipe( tap(res => {
          this.load(res);
        }), catchError(err =>
          of(new QueryResultsModel([], err))
        ), finalize(() => this.loadingSubject.next(false)) )
      .subscribe();
  }

  load(resultFromServer) {
    this.entitySubject.next(resultFromServer.items);
    this.paginatorTotalSubject.next(resultFromServer.totalCount);
  }
}
