export class ReinfPeriodoApuracaoModel {
  _id: string;
  dataInicial: Date;
  dataFinal: Date;
  situacaoPeriodo: string;
}

export class ReinfPeriodoApuracaoAnoMesModel {
  ano: Number;
  mes: Number;
}
