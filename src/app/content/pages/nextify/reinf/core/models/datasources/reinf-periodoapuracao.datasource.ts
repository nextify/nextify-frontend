import { of } from 'rxjs';
import { catchError, finalize, tap } from 'rxjs/operators';
import { QueryResultsModel } from '../../../../core/models/query-models/query-results.model';
import { BaseDataSource } from '../../../../core/models/datasources/base.datasource';
import { ReinfNotaFiscalService } from '../../services/notafiscal.service';
import { QueryParamsModel } from '../../../../core/models/query-models/query-params.model';
import { ListStateModel } from '../../utils/list-state.model';
import { ReinfPeriodoApuracaoService } from '../../services/periodoapuracao.service';

export class ReinfPeriodoApuracaoDataSource extends BaseDataSource {
  constructor(private periodoapuracaoService: ReinfPeriodoApuracaoService) {
    super();
  }

  filter(queryParams: QueryParamsModel, lastState: ListStateModel = null) {
    this.periodoapuracaoService.lastFilter$.next(queryParams);
    this.loadingSubject.next(true);
    // console.log(lastState);
    this.periodoapuracaoService
      .find(queryParams)
      .pipe( tap(res => {
          this.load(res);
        }), catchError(err =>
          of(new QueryResultsModel([], err))
        ), finalize(() => this.loadingSubject.next(false)) )
      .subscribe();
  }

  load(resultFromServer) {
    this.entitySubject.next(resultFromServer.items);
    this.paginatorTotalSubject.next(resultFromServer.totalCount);
  }
}
