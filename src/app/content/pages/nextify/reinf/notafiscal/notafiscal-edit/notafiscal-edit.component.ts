import { Component, OnInit, ChangeDetectionStrategy, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { BehaviorSubject, forkJoin, Observable, interval, Subscription } from 'rxjs';
import { map, startWith } from 'rxjs/operators';

import { ReinfNotaFiscalServicoModel } from '../../core/models/ReinfNotaFiscalServico.model';
import { ReinfNotaFiscalService } from '../../core/services/notafiscal.service';
import { ListStateModel } from '../../core/utils/list-state.model';
import { NumberUtilsService } from '../../../core/services/number-utils.service';
import { LayoutUtilsService, MessageType } from '../../../../../../core/services/layout-utils.service';
import { SubheaderService } from '../../../../../../core/services/layout/subheader.service';


@Component({
  selector: 'm-reinfnotafiscal-edit',
  templateUrl: './notafiscal-edit.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ReinfNotaFiscalEditComponent implements OnInit, OnDestroy {
  notaFiscal: ReinfNotaFiscalServicoModel;
  oldNotaFiscal: ReinfNotaFiscalServicoModel;
  selectedTab: number = 0;
  notafiscalForm: FormGroup;
  tiposervicoListState: ListStateModel;
  hasFormErrors: boolean = false;
  loadingSubject = new BehaviorSubject<boolean>(false);
  loading$ = this.loadingSubject.asObservable();

  // source = interval(1000);
  subscription: Subscription;

  tipoOperacaoList: any[] = [
    {value: '0', name: 'Entrada (Serviço Tomado)'},
    {value: '1', name: 'Saída (Serviço Prestado)'}
  ];

  constructor(
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private formBuilder: FormBuilder,
    private notafiscalService: ReinfNotaFiscalService,
    private layoutUtilsService: LayoutUtilsService,
    private subheaderService: SubheaderService,
    private numberUtils: NumberUtilsService
  ) {}

  ngOnInit() {
    this.loadingSubject.next(true);

    // const source = interval(10000);
    // this.subscription = source.subscribe(val => {
    //   if (!this.notafiscalForm.invalid && this.notaFiscal && this.notaFiscal._id) {
    //     this.onSumbit(false);
    //     console.log('salvo');
    //   }
    // });

    this.activatedRoute.queryParams.subscribe(params => {
      const id = params.id;
      if (id) {
        this.notafiscalService.getById(id).subscribe(res => {
          this.notaFiscal = res;
          this.oldNotaFiscal = Object.assign({}, res);
          this.initNotaFiscal();
        });
      } else {
        const newNotaFiscal = new ReinfNotaFiscalServicoModel();
        this.notaFiscal = newNotaFiscal;
        this.oldNotaFiscal = Object.assign({}, newNotaFiscal);
        this.initNotaFiscal();
      }
    });
  }

  ngOnDestroy() {
    // this.subscription.unsubscribe();
  }

  initNotaFiscal() {
    this.createForm();
    this.loadLists();
    this.loadingSubject.next(false);

    this.subheaderService.setTitle('EFD Reinf');
    if (!this.notaFiscal._id) {
      this.subheaderService.setBreadcrumbs([
        { title: 'EFD Reinf', page: '/reinf' },
        { title: 'Nota Fiscal', page: '/reinf/notafiscal' },
        { title: 'Cadastrar Nota Fiscal', page: '/reinf/notafiscal/add' }
      ]);
      return;
    }

    this.subheaderService.setBreadcrumbs([
      { title: 'EFD Reinf', page: '/reinf' },
      { title: 'Nota Fiscal', page: '/reinf/notafiscal' },
      { title: 'Editar Nota Fiscal', page: '/reinf/notafiscal/edit', queryParams: { id: this.notaFiscal._id } }
    ]);
  }

  createForm() {
    this.notafiscalForm = this.formBuilder.group({
      pessoa: [this.notaFiscal.pessoa, Validators.required],
      tipoOperacao: [this.notaFiscal.tipoOperacao, Validators.required],
      num: [this.notaFiscal.num, Validators.required],
      serie: [
        this.notaFiscal.serie,
        [Validators.required, Validators.maxLength(5)]
      ],
      dataEmissao: [this.notaFiscal.dataEmissao, Validators.required],
      vlrBruto: [
        this.numberUtils.numberFormat(this.notaFiscal.vlrBruto),
        [Validators.required]
      ],
      observacao: [this.notaFiscal.observacao]
    });
  }

  loadLists() {
    this.tiposervicoListState = new ListStateModel(this.notaFiscal._id);
  }

  onSumbit(withBack: boolean = false) {
    this.hasFormErrors = false;
    const controls = this.notafiscalForm.controls;
    /** check form */
    if (this.notafiscalForm.invalid) {
      Object.keys(controls).forEach(controlName =>
        controls[controlName].markAsTouched()
      );

      this.hasFormErrors = true;
      this.selectedTab = 0;
      return;
    }

    const editedNotaFiscal = this.prepare();

    if (editedNotaFiscal._id) {
      // console.log('UPDATE');
      this.updateNotaFiscal(editedNotaFiscal, withBack);
      return;
    }

    this.addNotaFiscal(editedNotaFiscal, withBack);
  }

  prepare(): ReinfNotaFiscalServicoModel {
    const controls = this.notafiscalForm.controls;
    const _notafiscal = new ReinfNotaFiscalServicoModel();
    // console.log(controls['pessoa'].value);

    _notafiscal._id = this.notaFiscal._id;
    _notafiscal.pessoa = controls['pessoa'].value['_id'];
    _notafiscal.tipoOperacao = controls['tipoOperacao'].value;
    _notafiscal.num = controls['num'].value;
    _notafiscal.serie = controls['serie'].value;
    _notafiscal.dataEmissao = controls['dataEmissao'].value;
    _notafiscal.vlrBruto = this.numberUtils.decimalParse(
      controls['vlrBruto'].value
    );
    _notafiscal.observacao = controls['observacao'].value;

    this.tiposervicoListState.prepareState();

    return _notafiscal;
  }

  addNotaFiscal(_notafiscal: ReinfNotaFiscalServicoModel, withBack: boolean = false) {
    this.loadingSubject.next(true);
    this.notafiscalService.create(_notafiscal).subscribe(res => {
      this.loadingSubject.next(false);
      if (withBack) {
        this.goBack(res._id);
      } else {
        const message = `Nota fiscal foi criada com sucesso.`;
        this.layoutUtilsService.showActionNotification(
          message,
          MessageType.Create,
          10000,
          true,
          false
        );
        this.refresh(res._id);
      }
    });
  }

  updateNotaFiscal(_notafiscal: ReinfNotaFiscalServicoModel, withBack: boolean = false) {
    this.loadingSubject.next(true);

    const tasks$ = [this.notafiscalService.update(_notafiscal)];

    this.tiposervicoListState.addedItems.forEach(element => {
      tasks$.push(this.notafiscalService.createTipoServico(element, this.tiposervicoListState));
    });

    this.tiposervicoListState.updatedItems.forEach(element => {
      tasks$.push(this.notafiscalService.updateTipoServico(element, this.tiposervicoListState));
    });

    this.tiposervicoListState.deletedItems.forEach(element => {
      tasks$.push(this.notafiscalService.deleteTipoServico(element, this.tiposervicoListState));
    });

    forkJoin(tasks$).subscribe(res => {
      this.loadingSubject.next(false);
      if (withBack) {
        this.goBack(_notafiscal._id);
      } else {
        const message = `Product successfully has been saved.`;
        this.layoutUtilsService.showActionNotification(
          message,
          MessageType.Update,
          10000,
          true,
          false
        );
        // console.log(res[1]);
        this.refresh(_notafiscal._id);
      }
    });
  }

  refresh(_id = '') {
    const _refreshUrl = 'reinf/notafiscal/edit?id=' + _id;
    this.router.navigateByUrl(_refreshUrl);
  }

  goBack(id = '0') {
    let _backUrl = 'reinf/notafiscal';
    if (id !== '0') {
      _backUrl += '?id=' + id;
    }
    this.router.navigateByUrl(_backUrl);
  }

  reset() {
    this.notaFiscal = Object.assign({}, this.oldNotaFiscal);
    this.createForm();
    this.hasFormErrors = false;
    this.notafiscalForm.markAsPristine();
    this.notafiscalForm.markAsUntouched();
    this.notafiscalForm.updateValueAndValidity();
  }

  getComponentTitle() {
    let result = 'Cadastrar Nota Fiscal';
    if (!this.notaFiscal || !this.notaFiscal._id) {
      return result;
    }

    result = `Editar Nota Fiscal - ${this.notaFiscal.num} - ${this.notaFiscal.serie}`;
    return result;
  }
}
