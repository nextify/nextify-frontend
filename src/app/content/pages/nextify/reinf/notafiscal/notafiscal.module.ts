import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { ReinfNotafiscalListComponent } from './notafiscal-list/notafiscal-list.component';
import { ReinfNotaFiscalEditComponent } from './notafiscal-edit/notafiscal-edit.component';
import { ReinfTipoServicoListComponent } from './subs/tiposervico-list/tiposervico-list.component';
import { ReinfTipoServicoEditComponent } from './subs/tiposervico-edit/tiposervico-edit.component';

import { NextifySharedModule } from '../../nextify-shared.module';
import { TwoDigitDecimaNumberDirective } from '../../../../../core/directives/two-digit-input.directive';
import { ReinfNotaFiscalService } from '../core/services/notafiscal.service';
import { HttpUtilsService } from '../../../../../core/services/http-utils.service';
import { NumberUtilsService } from '../../core/services/number-utils.service';

const routes: Routes = [
  {
    path: 'notafiscal',
    component: ReinfNotafiscalListComponent
  },
  {
    path: 'notafiscal/add',
    component: ReinfNotaFiscalEditComponent
  },
  {
    path: 'notafiscal/edit',
    component: ReinfNotaFiscalEditComponent
  },
  {
    path: 'notafiscal/edit/:id',
    component: ReinfNotaFiscalEditComponent
  }
];

@NgModule({
  imports: [
    FormsModule,
    ReactiveFormsModule,
    NextifySharedModule,
    RouterModule.forChild(routes)
  ],
  declarations: [
    ReinfNotafiscalListComponent,
    ReinfNotaFiscalEditComponent,
    ReinfTipoServicoListComponent,
    ReinfTipoServicoEditComponent
  ],
  entryComponents: [
    ReinfTipoServicoEditComponent
  ],
  providers: [
    ReinfNotaFiscalService,
    HttpUtilsService,
    NumberUtilsService
  ]
})
export class ReinfNotaFiscalModule {}
