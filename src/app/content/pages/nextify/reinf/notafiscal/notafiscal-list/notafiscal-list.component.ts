import { Component, OnInit, ElementRef, ViewChild, ChangeDetectionStrategy } from '@angular/core';
import { SelectionModel } from '@angular/cdk/collections';
import { ActivatedRoute } from '@angular/router';
// Material
import { MatPaginator, MatSort, MatDialog } from '@angular/material';

import { ReinfNotaFiscalDataSource } from '../../core/models/datasources/reinf-notafiscal.datasource';
import { ReinfNotaFiscalServicoModel } from '../../core/models/ReinfNotaFiscalServico.model';
import { ReinfNotaFiscalService } from '../../core/services/notafiscal.service';

import { QueryParamsModel } from '../../../core/models/query-models/query-params.model';

import { tap } from 'rxjs/operators';
import { merge } from 'rxjs';

@Component({
  selector: 'm-reinf-notafiscal.list',
  templateUrl: './notafiscal-list.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ReinfNotafiscalListComponent implements OnInit {
  dataSource: ReinfNotaFiscalDataSource;
  displayedColumns = ['select', 'num', 'serie', 'vlrBruto', 'actions'];
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  // Filter fields
  @ViewChild('searchInput') searchInput: ElementRef;
  filterStatus: string = '';
  filterCondition: string = '';

  selection = new SelectionModel<ReinfNotaFiscalServicoModel>(true, []);
  result: ReinfNotaFiscalServicoModel[] = [];

  constructor(private notafiscalService: ReinfNotaFiscalService,
              private route: ActivatedRoute) {}

  ngOnInit() {
    this.sort.sortChange.subscribe(() => (this.paginator.pageIndex = 0));

    merge(this.sort.sortChange, this.paginator.page)
      .pipe(
        tap(() => {
        this.loadList();
        })
      )
      .subscribe();

    this.dataSource = new ReinfNotaFiscalDataSource(this.notafiscalService);
    const queryParams = new QueryParamsModel({});

    // Read from URL itemId, for restore previous state
    this.route.queryParams.subscribe(params => {
      // if (params.id) {
      //   queryParams = this.notafiscalService.lastFilter$.getValue();
      //   this.restoreState(queryParams, +params.id);
      // }
      // First load
      this.dataSource.loadAll(queryParams);
    });
    this.dataSource.entitySubject.subscribe(res => this.result = res);
  }

  loadList() {
    const queryParams = new QueryParamsModel(
      this.filterConfiguration(),
      this.sort.direction,
      this.sort.active,
      this.paginator.pageIndex,
      this.paginator.pageSize
    );
    this.dataSource.loadAll(queryParams);
  }

  filterConfiguration(): any {
    const filter: any = {};
    const searchText: string = this.searchInput.nativeElement.value;

    if (this.filterStatus && this.filterStatus.length > 0) {
      filter.status = +this.filterStatus;
    }

    if (this.filterCondition && this.filterCondition.length > 0) {
      filter.condition = +this.filterCondition;
    }

    filter.model = searchText;

    filter.manufacture = searchText;
    filter.color = searchText;
    filter.VINCode = searchText;
    return filter;
  }

  /** SELECTION */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.result.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    if (this.isAllSelected()) {
      this.selection.clear();
    } else {
      this.result.forEach(row => this.selection.select(row));
    }
  }

}
