import { Component, OnInit, ElementRef, ViewChild, Input, ChangeDetectionStrategy } from '@angular/core';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
// Material
import { MatPaginator, MatSort, MatDialog } from '@angular/material';
import { SelectionModel } from '@angular/cdk/collections';

// RXJS
import { debounceTime, distinctUntilChanged, tap } from 'rxjs/operators';
import { fromEvent, merge, BehaviorSubject } from 'rxjs';
import { ListStateModel, StateActions } from '../../../core/utils/list-state.model';
import { ReinfNotaFiscalServicoItemModel, ReinfNotaFiscalServicoItemINSS } from '../../../core/models/ReinfNotaFiscalServicoItem.model';
import { ReinfNotaFiscalTipoServicoDataSource } from '../../../core/models/datasources/reinf-tiposervico.datasource';
import { ReinfNotaFiscalService } from '../../../core/services/notafiscal.service';
import { QueryParamsModel } from '../../../../core/models/query-models/query-params.model';
import { MessageType, LayoutUtilsService } from '../../../../../../../core/services/layout-utils.service';

// Components
import { ReinfTipoServicoEditComponent } from '../tiposervico-edit/tiposervico-edit.component';
// tpServico: string;
// vlrBaseRet: number;
// vlrRetencao: number;

@Component({
  selector: 'm-reinftiposervico-list',
  templateUrl: './tiposervico-list.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ReinfTipoServicoListComponent implements OnInit {
  // Incoming data
  @Input() loadingSubject = new BehaviorSubject<boolean>(false);
  @Input() tiposervicoListState: ListStateModel;

  // Table fields
  dataSource: ReinfNotaFiscalTipoServicoDataSource;
  displayedColumns = [
    'select',
    // 'id',
    'tipoServico',
    'vlrBaseRetencao',
    'vlrRetencao',
    'actions'
  ];
  @ViewChild(MatPaginator)
  paginator: MatPaginator;
  @ViewChild(MatSort)
  sort: MatSort;

  // Filter fields
  @ViewChild('searchInput')
  searchInput: ElementRef;

  // Selection
  selection = new SelectionModel<ReinfNotaFiscalServicoItemModel>(true, []);
  tiposervicoResult: ReinfNotaFiscalServicoItemModel[] = [];

  // Add and Edit
  isSwitchedToEditMode: boolean = false;
  loadingAfterSubmit: boolean = false;
  formGroup: FormGroup;
  itemForEdit: ReinfNotaFiscalServicoItemModel;
  itemForAdd: ReinfNotaFiscalServicoItemModel;

  // Listas
  tipoServicoList: any[] = [
    {value: '000000001', name: 'Tipo Servico 1'},
    {value: '000000002', name: 'Tipo Servico 2'}
  ];

  constructor(
    private formBuilder: FormBuilder,
    private notafiscalService: ReinfNotaFiscalService,
    private layoutUtilsService: LayoutUtilsService,
    public dialog: MatDialog,
  ) {}

  ngOnInit() {
    this.sort.sortChange.subscribe(() => (this.paginator.pageIndex = 0));

    merge(this.sort.sortChange, this.paginator.page)
      .pipe(
        tap(() => {
          this.loadTipoServicoList();
        })
      )
      .subscribe();

    // fromEvent(this.searchInput.nativeElement, 'keyup')
    //   .pipe(
    //     debounceTime(150),
    //     distinctUntilChanged(),
    //     tap(() => {
    //       this.paginator.pageIndex = 0;
    //       this.loadTipoServicoList();
    //     })
    //   )
    //   .subscribe();
    // Init DataSource
    this.dataSource = new ReinfNotaFiscalTipoServicoDataSource(
      this.notafiscalService
    );
    // this loading binded to parent component loading
    this.dataSource.loading$.subscribe(res => {
      this.loadingSubject.next(res);
    });
    this.loadTipoServicoList(true);
    this.dataSource.entitySubject.subscribe(
      res => (this.tiposervicoResult = res)
    );
    this.createFormGroup();
  }

  // Loading
  loadTipoServicoList(_isFirstLoading: boolean = false) {
    let queryParams = new QueryParamsModel(
      this.filterConfiguration(),
      this.sort.direction,
      this.sort.active,
      this.paginator.pageIndex,
      this.paginator.pageSize
    );
    if (_isFirstLoading) {
      queryParams = new QueryParamsModel(
        this.filterConfiguration(),
        'asc',
        'id',
        0,
        9
      );
    }
    this.dataSource.loadAll(queryParams, this.tiposervicoListState);
  }

  createFormGroup(_item = null) {
    // 'edit' prefix - for item editing
    // 'add' prefix - for item creation
    this.formGroup = this.formBuilder.group({
      editTpServico: ['', Validators.required],
      editVlrBaseRet: ['0', Validators.required],
      editVlrRetencao: ['0', Validators.required],

      newTpServico: ['', Validators.required],
      newVlrBaseRet: ['0', Validators.required],
      newVlrRetencao: ['0', Validators.required]
    });
    this.clearAddForm();
    this.clearEditForm();
  }

  clearAddForm() {
    const controls = this.formGroup.controls;
    controls['newTpServico'].setValue('');
    controls['newTpServico'].markAsPristine();
    controls['newTpServico'].markAsUntouched();

    controls['newVlrBaseRet'].setValue('0');
    controls['newVlrBaseRet'].markAsPristine();
    controls['newVlrBaseRet'].markAsUntouched();

    controls['newVlrRetencao'].setValue('0');
    controls['newVlrRetencao'].markAsPristine();
    controls['newVlrRetencao'].markAsUntouched();

    this.itemForAdd = new ReinfNotaFiscalServicoItemModel();
    this.itemForAdd.clear(this.tiposervicoListState.entityId);
    this.itemForAdd._isEditMode = false;
  }

  checkAddForm() {
    const controls = this.formGroup.controls;
    if (
      controls['newTpServico'].invalid ||
      controls['newVlrBaseRet'].invalid ||
      controls['newVlrRetencao'].invalid
    ) {
      controls['newTpServico'].markAsTouched();
      controls['newVlrBaseRet'].markAsTouched();
      controls['newVlrRetencao'].markAsTouched();
      return false;
    }
    return true;
  }

  addItemButtonOnClick() {
    this.clearAddForm();
    this.itemForAdd._isEditMode = true;
    this.isSwitchedToEditMode = true;
  }

  cancelAddButtonOnClick() {
    this.itemForAdd._isEditMode = false;
    this.isSwitchedToEditMode = false;
  }

  saveNewItem() {
    if (!this.checkAddForm()) {
      return;
    }

    this.loadingAfterSubmit = true;
    const controls = this.formGroup.controls;
    /* Server loading imitation. Remove 'setTemout' on real code */
    setTimeout(() => {
      this.loadingAfterSubmit = false;
      this.itemForAdd._isEditMode = false;
      this.itemForAdd.tipoServico = controls['newTpServico'].value;
      this.itemForAdd.inss.vlrBaseRetencao = +controls['newVlrBaseRet'].value;
      this.itemForAdd.inss.vlrRetencao = +controls['newVlrRetencao'].value;
      // this.itemForAdd._updatedDate = this.typesUtilsService.getDateStringFromDate();
      this.itemForAdd._createdDate = this.itemForEdit._updatedDate;
      this.itemForAdd._userId = 1; // Admin TODO: Get from user servics
      this.tiposervicoListState.setItem(this.itemForAdd, StateActions.CREATE);
      this.loadTipoServicoList();
      const _saveMessage = `Item has been created`;
      this.isSwitchedToEditMode = false;
      this.layoutUtilsService.showActionNotification(_saveMessage, MessageType.Create, 10000, true, false);
      this.clearAddForm();
    }, 300);
  }

  clearEditForm() {
    const controls = this.formGroup.controls;
    controls['editTpServico'].setValue('');
    // controls['editTpServico'].markAsPristine();
    // controls['editTpServico'].markAsUntouched();

    controls['editVlrBaseRet'].setValue('0');
    // controls['editVlrBaseRet'].markAsPristine();
    // controls['editVlrBaseRet'].markAsUntouched();

    controls['editVlrRetencao'].setValue('0');
    // controls['editVlrRetencao'].markAsPristine();
    // controls['editVlrRetencao'].markAsUntouched();

    this.itemForEdit = new ReinfNotaFiscalServicoItemModel();
    this.itemForEdit.clear(this.tiposervicoListState.entityId);
    this.itemForEdit._isEditMode = false;
  }

  checkEditForm() {
    const controls = this.formGroup.controls;
    if (
      controls['editTpServico'].invalid ||
      controls['editVlrBaseRet'].invalid ||
      controls['editVlrRetencao'].invalid
    ) {
      return false;
    }

    return true;
  }

  editItemButtonOnClick(_item: ReinfNotaFiscalServicoItemModel) {
    const controls = this.formGroup.controls;
    controls['editTpServico'].setValue(_item.tipoServico);
    controls['editVlrBaseRet'].setValue(_item.inss.vlrBaseRetencao.toString());
    controls['editVlrRetencao'].setValue(_item.inss.vlrRetencao.toString());
    _item._isEditMode = true;
    this.isSwitchedToEditMode = true;
  }

  cancelEditButtonOnClick(_item: ReinfNotaFiscalServicoItemModel) {
    _item._isEditMode = false;
    this.isSwitchedToEditMode = false;
  }

  saveUpdatedItem(_item: ReinfNotaFiscalServicoItemModel) {
    if (!this.checkEditForm()) {
      return;
    }
    this.loadingAfterSubmit = true;
    const controls = this.formGroup.controls;
    this.loadingAfterSubmit = false;
    _item.tipoServico = controls['editTpServico'].value;
    _item.inss.vlrBaseRetencao = +controls['editVlrBaseRet'].value;
    _item.inss.vlrRetencao = +controls['editVlrRetencao'].value;

    _item._isEditMode = false;
    this.tiposervicoListState.setItem(_item, StateActions.UPDATE);
    const saveMessage = 'Item foi atualizado.';
    this.isSwitchedToEditMode = false;
    console.log(this.tiposervicoListState);
    this.layoutUtilsService.showActionNotification(saveMessage, MessageType.Update, 10000, true, false);
  }

  /** FILTRATION */
  filterConfiguration(): any {
    const filter: any = {};
    // const searchText: string = this.searchInput.nativeElement.value;

    // filter.text = searchText;
    return filter;
  }

  /** SELECTION */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.tiposervicoResult.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    if (this.isAllSelected()) {
      this.selection.clear();
    } else {
      this.tiposervicoResult.forEach(row => this.selection.select(row));
    }
  }

  /** ACTIONS */
  /** Delete */
  deleteItem(_item: ReinfNotaFiscalServicoItemModel) {
    const _title: string = 'Remark Delete';
    const _description: string =
      'Are you sure to permanently delete this remark?';
    const _waitDesciption: string = 'Remark is deleting...';
    const _deleteMessage = `Remark has been deleted`;

    const dialogRef = this.layoutUtilsService.deleteElement(
      _title,
      _description,
      _waitDesciption
    );
    dialogRef.afterClosed().subscribe(res => {
      if (!res) {
        return;
      }

      _item._isDeleted = true;
      this.tiposervicoListState.setItem(_item, StateActions.DELETE);
      this.layoutUtilsService.showActionNotification(
        _deleteMessage,
        MessageType.Delete
      );
      this.loadTipoServicoList();
    });
  }

  deleteItems() {
    const _title: string = 'Remarks Delete';
    const _description: string = 'Are you sure to permanently delete selected remarks?';
    const _waitDesciption: string = 'Remarks are deleting...';
    const _deleteMessage = 'Selected remarks have been deleted';

    const dialogRef = this.layoutUtilsService.deleteElement(_title, _description, _waitDesciption);
    dialogRef.afterClosed().subscribe(res => {
      if (!res) {
        return;
      }

      const length = this.selection.selected.length;
      for (let i = 0; i < length; i++) {
        this.selection.selected[i]._isDeleted = true;
        this.tiposervicoListState.setItem(this.selection.selected[i], StateActions.DELETE);
      }
      this.layoutUtilsService.showActionNotification(_deleteMessage, MessageType.Delete);
      this.loadTipoServicoList();
      this.selection.clear();
    });
  }

  /** Fetch */
  fetchItems() {
    const messages = [];
    this.selection.selected.forEach(elem => { messages.push({ text: `${elem.tipoServico}`, id: elem._id }); });
    this.layoutUtilsService.fetchElements(messages);
  }

  addItem() {
    let _item = new ReinfNotaFiscalServicoItemModel();
    _item.clear(this.tiposervicoListState.entityId);

		const dialogRef = this.dialog.open(ReinfTipoServicoEditComponent, {
			data: {
				_tipoServico: _item,
        tipoServicoList: this.tipoServicoList,
				isNew: true
			},
			width: '750px'
		});

		dialogRef.afterClosed().subscribe(res => {
			if (res && res.isUpdated) {
				_item = res._tipoServico;

				this.tiposervicoListState.setItem(_item, StateActions.CREATE);
        this.loadTipoServicoList();
				const saveMessage = `O item de serviço criado.`;
				this.layoutUtilsService.showActionNotification(saveMessage, MessageType.Update, 10000, true, false);
			}
		});
	}

  editItem(_item: ReinfNotaFiscalServicoItemModel) {
		const dialogRef = this.dialog.open(ReinfTipoServicoEditComponent, {
			data: {
				_tipoServico: _item,
        tipoServicoList: this.tipoServicoList,
				isNew: false
			},
			width: '750px'
		});

		dialogRef.afterClosed().subscribe(res => {
			if (res && res.isUpdated) {
				_item = res._tipoServico;

				this.tiposervicoListState.setItem(_item, StateActions.UPDATE);
        this.loadTipoServicoList();
        console.log(this.tiposervicoListState);
				const saveMessage = `O item de serviço foi atualizado.`;
				this.layoutUtilsService.showActionNotification(saveMessage, MessageType.Update, 10000, true, false);
			}
		});
	}

  /* UI **/
  getTypeStr(value: any, array: any[]): string {
    return this.tipoServicoList.filter(n => n.value === value)[0]['name'];
  }
}

