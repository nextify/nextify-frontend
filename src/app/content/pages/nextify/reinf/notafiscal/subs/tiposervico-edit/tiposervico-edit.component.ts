import {Component, Inject, Input, OnInit} from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormControl, Validators, FormBuilder, FormGroup } from '@angular/forms';
import * as _ from 'lodash';

import { NumberUtilsService } from '../../../../core/services/number-utils.service';

import { ReinfNotaFiscalServicoItemModel } from '../../../core/models/ReinfNotaFiscalServicoItem.model';

@Component({
  selector: 'm-reinftiposervico-edit',
  templateUrl: './tiposervico-edit.component.html',
})
export class ReinfTipoServicoEditComponent implements OnInit {
	// selectedNameIdForUpdate = new FormControl('', Validators.required);
	// selectedValueForUpdate = new FormControl('', Validators.required);
	// tiposervicoModel: ReinfNotaFiscalTipoServicoModel[] = [];

  tipoServico: ReinfNotaFiscalServicoItemModel;
	viewLoading: boolean = false;
	loadingAfterSubmit: boolean = false;
  formGroup: FormGroup;
// Listas
  tipoServicoList: any[] = [];

	constructor(
		public dialogRef: MatDialogRef<ReinfTipoServicoEditComponent>,
		@Inject(MAT_DIALOG_DATA) public data: any,
    private formBuilder: FormBuilder,
    private numberUtils: NumberUtilsService
	) {
    this.tipoServicoList = this.data.tipoServicoList;
    this.tipoServico = this.data._tipoServico;
	}

  	ngOnInit() {
		/* Server loading imitation. Remove this on real code */
    this.createFormGroup();
		this.viewLoading = true;
		setTimeout(() => {
			this.viewLoading = false;
		}, 500);
	}

  onNoClick(): void {
		this.dialogRef.close({ isUpdated : false });
	}

	save() {
    const controls = this.formGroup.controls;

    if (this.formGroup.invalid) {
      Object.keys(controls).forEach(controlName =>
        controls[controlName].markAsTouched()
      );
      return;
    }

		this.loadingAfterSubmit = true;
		this.viewLoading = true;

		this.prepare();
		/* Server loading imitation. Remove this on real code */
		setTimeout(() => {
			this.viewLoading = false;
			this.closeDialog(this.tipoServico);
		}, 500);
	}

  prepare(): void {
    const controls = this.formGroup.controls;

    this.tipoServico.tipoServico = controls['tpServico'].value;
    this.tipoServico.inss.aliquota = this.numberUtils.decimalParse(controls['aliquota'].value);
    this.tipoServico.inss.vlrBaseRetencao = this.numberUtils.decimalParse(controls['vlrBaseRet'].value);
    this.tipoServico.inss.vlrRetencao = this.numberUtils.decimalParse(controls['vlrRetencao'].value);
    this.tipoServico.inss.vlrRetSub = this.numberUtils.decimalParse(controls['vlrRetSub'].value);
    this.tipoServico.inss.vlrNRetPrinc = this.numberUtils.decimalParse(controls['vlrNRetPrinc'].value);
    this.tipoServico.inss.vlrServicos15 = this.numberUtils.decimalParse(controls['vlrServicos15'].value);
    this.tipoServico.inss.vlrServicos20 = this.numberUtils.decimalParse(controls['vlrServicos20'].value);
    this.tipoServico.inss.vlrServicos25 = this.numberUtils.decimalParse(controls['vlrServicos25'].value);
    this.tipoServico.inss.vlrAdicional = this.numberUtils.decimalParse(controls['vlrAdicional'].value);
    this.tipoServico.inss.vlrNRetAdic = this.numberUtils.decimalParse(controls['vlrNRetAdic'].value);

  }

  createFormGroup() {
    this.formGroup = this.formBuilder.group({
      tpServico: [this.tipoServico.tipoServico, Validators.required],
      vlrBaseRet: [this.numberUtils.numberFormat(this.tipoServico.inss.vlrBaseRetencao), Validators.required],
      aliquota: [this.numberUtils.numberFormat(this.tipoServico.inss.aliquota), Validators.required],
      vlrRetencao: [this.numberUtils.numberFormat(this.tipoServico.inss.vlrRetencao), Validators.required],
      vlrRetSub: [this.numberUtils.numberFormat(this.tipoServico.inss.vlrRetSub)],
      vlrNRetPrinc: [this.numberUtils.numberFormat(this.tipoServico.inss.vlrNRetPrinc)],
      vlrServicos15: [this.numberUtils.numberFormat(this.tipoServico.inss.vlrServicos15)],
      vlrServicos20: [this.numberUtils.numberFormat(this.tipoServico.inss.vlrServicos20)],
      vlrServicos25: [this.numberUtils.numberFormat(this.tipoServico.inss.vlrServicos25)],
      vlrAdicional: [this.numberUtils.numberFormat(this.tipoServico.inss.vlrAdicional)],
      vlrNRetAdic: [this.numberUtils.numberFormat(this.tipoServico.inss.vlrNRetAdic)],
    });
  }

  closeDialog(_item: ReinfNotaFiscalServicoItemModel) {
		this.dialogRef.close({
      isUpdated: true,
			_tipoServico: _item
		});
	}

	// getSpecificationsNameById(specId: number) {
	// 	const s_index = _.findIndex(this.specificationsTypes, function (o) { return o.id === specId; });
	// 	if (s_index > -1) {
	// 		return this.specificationsTypes[s_index].name;
	// 	}

	// 	return '';
	// }

  getTypeStr(value: any, array: any[]): string {
    return this.tipoServicoList.filter(n => n.value === value)[0]['name'];
  }

	checkDataIsValid(): boolean {
		return this.formGroup.valid;
	}
}
