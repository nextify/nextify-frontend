import { NgModule } from '@angular/core';

import { ReinfRoutingModule } from './reinf-routing.module';
import { ReinfNotaFiscalModule } from './notafiscal/notafiscal.module';
import { ReinfPeriodoApuracaoModule } from './periodoapuracao/periodoapuracao.module';

@NgModule({
  declarations: [],
  imports: [
    ReinfRoutingModule,
    ReinfNotaFiscalModule,
    ReinfPeriodoApuracaoModule
    ],
  providers: []
})
export class ReinfModule {}

