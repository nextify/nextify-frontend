import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

// import { ReinfNotafiscalListComponent } from './notafiscal-list/notafiscal-list.component';
// import { ReinfNotaFiscalEditComponent } from './notafiscal-edit/notafiscal-edit.component';
// import { ReinfTipoServicoListComponent } from './subs/tiposervico-list/tiposervico-list.component';
// import { ReinfTipoServicoEditComponent } from './subs/tiposervico-edit/tiposervico-edit.component';

import { NextifySharedModule } from '../../nextify-shared.module';
import { TwoDigitDecimaNumberDirective } from '../../../../../core/directives/two-digit-input.directive';
import { ReinfNotaFiscalService } from '../core/services/notafiscal.service';
import { HttpUtilsService } from '../../../../../core/services/http-utils.service';
import { NumberUtilsService } from '../../core/services/number-utils.service';

import { PeriodoApuracaoListComponent } from './periodoapuracao-list/periodoapuracao-list.component';
import { ReinfPeriodoApuracaoService } from '../core/services/periodoapuracao.service';
import { PeriodoApuracaoEditComponent } from './periodoapuracao-edit/periodoapuracao-edit.component';

const routes: Routes = [
  {
    path: 'periodoapuracao',
    component: PeriodoApuracaoListComponent
  }
];

@NgModule({
  imports: [
    FormsModule,
    ReactiveFormsModule,
    NextifySharedModule,
    RouterModule.forChild(routes)
  ],
  declarations: [
    PeriodoApuracaoListComponent,
    PeriodoApuracaoEditComponent
  ],
  entryComponents: [
    PeriodoApuracaoEditComponent
  ],
  providers: [
    ReinfPeriodoApuracaoService,
    HttpUtilsService,
    NumberUtilsService
  ]
})
export class ReinfPeriodoApuracaoModule {}
