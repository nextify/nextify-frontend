import {Component, Inject, Input, OnInit} from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { MatDatepicker } from '@angular/material/datepicker';
import { MAT_DATE_FORMATS } from '@angular/material/core';
import { FormControl, Validators, FormBuilder, FormGroup } from '@angular/forms';
import * as _ from 'lodash';
import * as _moment from 'moment';

// import { ReinfNotaFiscalServicoItemModel } from '../../../core/models/ReinfNotaFiscalServicoItem.model';
const moment = _rollupMoment || _moment;
// tslint:disable-next-line:no-duplicate-imports
import {default as _rollupMoment, Moment} from 'moment';
import { ReinfPeriodoApuracaoAnoMesModel } from '../../core/models/reinf-periodoapuracao.model';
import { LayoutUtilsService } from '../../../../../../core/services/layout-utils.service';

export const MY_FORMATS = {
  parse: {
    dateInput: 'MM/YYYY',
  },
  display: {
    dateInput: 'MM/YYYY',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY',
  },
};

@Component({
  selector: 'm-reinf-periodoapuracao-edit',
  templateUrl: './periodoapuracao-edit.component.html',
  providers: [
    // `MomentDateAdapter` can be automatically provided by importing `MomentDateModule` in your
    // application's root module. We provide it at the component level here, due to limitations of
    // our example generation script.
    {provide: MAT_DATE_FORMATS, useValue: MY_FORMATS},
  ],
})
export class PeriodoApuracaoEditComponent implements OnInit {
  periodoApuracao: ReinfPeriodoApuracaoAnoMesModel;
	viewLoading: boolean = false;
	loadingAfterSubmit: boolean = false;
  formGroup: FormGroup;

	constructor(
		public dialogRef: MatDialogRef<PeriodoApuracaoEditComponent>,
		@Inject(MAT_DIALOG_DATA) public data: any,
    private formBuilder: FormBuilder
	) {
    this.periodoApuracao = this.data._periodoApuracao;
	}

  	ngOnInit() {
		/* Server loading imitation. Remove this on real code */
    this.createFormGroup();
		this.viewLoading = true;
		setTimeout(() => {
			this.viewLoading = false;
		}, 500);
	}

  chosenYearHandler(normalizedYear: Moment) {
    const controls = this.formGroup.controls;
    const ctrlValue = controls['periodoApuracao'].value;
    ctrlValue.year(normalizedYear.year());
    controls['periodoApuracao'].setValue(ctrlValue);
    this.periodoApuracao.ano = normalizedYear.year();
  }

  chosenMonthHandler(normalizedMonth: Moment, datepicker: MatDatepicker<Moment>) {
    const controls = this.formGroup.controls;
    const ctrlValue = controls['periodoApuracao'].value;
    ctrlValue.month(normalizedMonth.month());
    controls['periodoApuracao'].setValue(ctrlValue);
    this.periodoApuracao.mes = normalizedMonth.month() + 1; // moment.month() is zero based
    datepicker.close();
  }

  onNoClick(): void {
		this.dialogRef.close({ isUpdated : false });
	}

	save() {
    const controls = this.formGroup.controls;

    if (this.formGroup.invalid) {
      Object.keys(controls).forEach(controlName =>
        controls[controlName].markAsTouched()
      );
      return;
    }

		this.loadingAfterSubmit = true;
		this.viewLoading = true;

		/* Server loading imitation. Remove this on real code */
		setTimeout(() => {
			this.viewLoading = false;
			this.closeDialog(this.periodoApuracao);
		}, 500);
	}

  createFormGroup() {
    this.formGroup = this.formBuilder.group({
      periodoApuracao: [moment(), Validators.required],
    });

    this.periodoApuracao.mes = moment().month() + 1; // moment.month() is zero based
    this.periodoApuracao.ano = moment().year();

  }

  closeDialog(_item: ReinfPeriodoApuracaoAnoMesModel) {
		this.dialogRef.close({
      isUpdated: true,
			_periodoApuracao: _item
		});
	}

	// getSpecificationsNameById(specId: number) {
	// 	const s_index = _.findIndex(this.specificationsTypes, function (o) { return o.id === specId; });
	// 	if (s_index > -1) {
	// 		return this.specificationsTypes[s_index].name;
	// 	}

	// 	return '';
	// }

  // getTypeStr(value: any, array: any[]): string {
  //   return this.tipoServicoList.filter(n => n.value === value)[0]['name'];
  // }

	checkDataIsValid(): boolean {
		return this.formGroup.valid;
	}
}
