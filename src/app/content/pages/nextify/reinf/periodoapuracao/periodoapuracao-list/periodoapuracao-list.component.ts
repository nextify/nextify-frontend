import { Component, OnInit, ElementRef, ViewChild, ChangeDetectionStrategy } from '@angular/core';
import { SelectionModel } from '@angular/cdk/collections';
import { ActivatedRoute } from '@angular/router';
// Material
import { MatPaginator, MatSort, MatDialog } from '@angular/material';

import { ReinfNotaFiscalDataSource } from '../../core/models/datasources/reinf-notafiscal.datasource';
import { ReinfNotaFiscalServicoModel } from '../../core/models/ReinfNotaFiscalServico.model';
import { ReinfNotaFiscalService } from '../../core/services/notafiscal.service';

import { QueryParamsModel } from '../../../core/models/query-models/query-params.model';

import { tap } from 'rxjs/operators';
import { merge } from 'rxjs';
import { ReinfPeriodoApuracaoDataSource } from '../../core/models/datasources/reinf-periodoapuracao.datasource';
import { ReinfPeriodoApuracaoModel, ReinfPeriodoApuracaoAnoMesModel } from '../../core/models/reinf-periodoapuracao.model';
import { ReinfPeriodoApuracaoService } from '../../core/services/periodoapuracao.service';
import { PeriodoApuracaoEditComponent } from '../periodoapuracao-edit/periodoapuracao-edit.component';
import { LayoutUtilsService, MessageType } from '../../../../../../core/services/layout-utils.service';

@Component({
  selector: 'm-reinf-periodoapuracao-list',
  templateUrl: './periodoapuracao-list.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PeriodoApuracaoListComponent implements OnInit {
  dataSource: ReinfPeriodoApuracaoDataSource;
  displayedColumns = ['dataInicial', 'dataFinal', 'situacaoPeriodo', 'actions'];
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  // Filter fields
  @ViewChild('searchInput') searchInput: ElementRef;
  filterStatus: string = '';
  filterCondition: string = '';

  selection = new SelectionModel<ReinfPeriodoApuracaoModel>(true, []);
  result: ReinfPeriodoApuracaoModel[] = [];

  constructor(private periodoapuracaoService: ReinfPeriodoApuracaoService,
              private layoutUtilsService: LayoutUtilsService,
              private route: ActivatedRoute,
              public dialog: MatDialog) {}

  ngOnInit() {
    this.sort.sortChange.subscribe(() => (this.paginator.pageIndex = 0));
    merge(this.sort.sortChange, this.paginator.page)
      .pipe(
        tap(() => {
        this.loadList();
        })
      )
      .subscribe();

    this.dataSource = new ReinfPeriodoApuracaoDataSource(this.periodoapuracaoService);
    const queryParams = new QueryParamsModel({});
    queryParams.sortField = 'dataInicial';
    queryParams.sortOrder = 'desc';

    // Read from URL itemId, for restore previous state
    this.route.queryParams.subscribe(params => {
      // if (params.id) {
      //   queryParams = this.notafiscalService.lastFilter$.getValue();
      //   this.restoreState(queryParams, +params.id);
      // }
      // First load
      this.dataSource.filter(queryParams);
    });
    this.dataSource.entitySubject.subscribe(res => this.result = res);
  }

  loadList() {
    const queryParams = new QueryParamsModel(
      this.filterConfiguration(),
      this.sort.direction,
      this.sort.active,
      this.paginator.pageIndex,
      this.paginator.pageSize
    );
    this.dataSource.filter(queryParams);
  }

  filterConfiguration(): any {
    const filter: any = {};
    // const searchText: string = this.searchInput.nativeElement.value;

    // if (this.filterStatus && this.filterStatus.length > 0) {
    //   filter.status = +this.filterStatus;
    // }

    // if (this.filterCondition && this.filterCondition.length > 0) {
    //   filter.condition = +this.filterCondition;
    // }

    // filter.model = searchText;

    // filter.manufacture = searchText;
    // filter.color = searchText;
    // filter.VINCode = searchText;
    return filter;
  }

  /** SELECTION */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.result.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    if (this.isAllSelected()) {
      this.selection.clear();
    } else {
      this.result.forEach(row => this.selection.select(row));
    }
  }

  addItem() {
    let _item = new ReinfPeriodoApuracaoAnoMesModel();
    // _item.clear(this.tiposervicoListState.entityId);

		const dialogRef = this.dialog.open(PeriodoApuracaoEditComponent, {
			data: {
				_periodoApuracao: _item,
				isNew: true
			},
			width: '750px'
		});

		dialogRef.afterClosed().subscribe(res => {
			if (res && res.isUpdated) {
				_item = res._periodoApuracao;

        this.periodoapuracaoService.create(_item).subscribe(response => {
          const saveMessage = `Período de Escrituração criado com sucesso.`;
          this.layoutUtilsService.showActionNotification(saveMessage, MessageType.Update, 10000, true, false);
          setTimeout(this.loadList(), 0);

        }, response => {
          this.layoutUtilsService.showActionNotification(response.error.message, MessageType.Update, 10000, true, false);
        });

				// this.tiposervicoListState.setItem(_item, StateActions.CREATE);
        // this.loadTipoServicoList();
				// const saveMessage = `O item de serviço criado.`;
				// this.layoutUtilsService.showActionNotification(saveMessage, MessageType.Update, 10000, true, false);
			}
		});
	}

}
