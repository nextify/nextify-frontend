import { Directive, AfterContentInit, Input, Self, Host } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatAutocomplete, MatAutocompleteTrigger, MatAutocompleteSelectedEvent } from '@angular/material';
import { Subscription } from 'rxjs';

@Directive({
  selector: '[enforceInput]'
})
export class EnforceInputDirective implements AfterContentInit {

  @Input() formControl: FormControl;
  @Input() matAutocomplete: MatAutocomplete;

  subscription: Subscription;

  constructor(@Host() @Self() private readonly autoCompleteTrigger: MatAutocompleteTrigger) {
  }

  ngAfterContentInit() {
    if (this.formControl === undefined) {
      throw Error('inputCtrl @Input should be provided ');
    }

    if (this.matAutocomplete === undefined) {
      throw Error('valueCtrl @Input should be provided ');
    }

    setTimeout(() => {
      this.subscribeToClosingActions();
      this.handelSelection();
    }, 0);
  }

  private subscribeToClosingActions(): void {
    if (this.subscription && !this.subscription.closed) {
      this.subscription.unsubscribe();
    }

    this.subscription = this.autoCompleteTrigger.panelClosingActions
      .subscribe((e) => {
        if (!e || !e.source) {
          const selected = this.matAutocomplete.options
            .map(option => option.value)
            .find(option => option === this.formControl.value);

          if (selected == null) {
            this.formControl.setValue(null);
          }
        }
      },
        err => this.subscribeToClosingActions(),
        () => this.subscribeToClosingActions()
      );
  }

  private handelSelection() {
    this.matAutocomplete.optionSelected.subscribe(
      (e: MatAutocompleteSelectedEvent) => {
        this.formControl.setValue(e.option.value);
      }
    );
  }
}
