import { FormControl } from '@angular/forms';
import { Observable, Subscription } from 'rxjs';
import { Component, Input, AfterViewInit } from '@angular/core';
import { switchMap, debounceTime, filter } from 'rxjs/operators';

import { PessoaService } from '../services/pessoa.service';
import { PessoaModel } from '../models/pessoa.model';
import { QueryParamsModel } from '../models/query-models/query-params.model';

@Component({
  selector: 'm-n-pessoa-autocomplete',
  template: `
  <mat-form-field class="mat-form-field-fluid">
    <input matInput autoClose
             type="text" placeholder="Assignee" aria-label="Assignee" [formControl]="control" [matAutocomplete]="auto">
    <button mat-button matSuffix mat-icon-button aria-label="Clear" (click)="control.setValue('')">
    <mat-icon>close</mat-icon>
  </button>
    <mat-autocomplete #auto="matAutocomplete" [displayWith]="displayFn">
      <mat-option *ngFor="let option of filteredOptions | async" [value]="option">
        {{ option.nome }}
      </mat-option>
    </mat-autocomplete>
  </mat-form-field>
  `
})
export class PessoaAutocompleteComponent implements AfterViewInit {
  @Input()
  control: FormControl = new FormControl();
  filteredOptions: Observable<PessoaModel[]>;
  subscription: Subscription;

  constructor(private pessoaService: PessoaService) {}

  ngAfterViewInit() {
    this.filteredOptions = this.control.valueChanges.pipe(
      filter((value: string) => value && value.length >= 3),
      debounceTime(300),
      switchMap(value => this._filter(value))
    );
  }

  displayFn(pessoa?: any): string | undefined {
    return pessoa ? pessoa.nome : undefined;
  }

  private _filter(val: string) {
    // console.log('_filter:', val);
    const query = new QueryParamsModel(val);
    return this.pessoaService.find(query);
  }
}
