export class QueryResultsModel {
	// fields
	items: any[];
	totalCount: number;
	errorMessage: string;

	constructor(_items: any[] = [], _items2: any[] = null, _errorMessage: string = '') {

    if (_items2 !== null) {
      this.items = [..._items, ..._items2];
    } else {
      this.items = _items;
    }
		this.totalCount = _items.length;
	}
}
