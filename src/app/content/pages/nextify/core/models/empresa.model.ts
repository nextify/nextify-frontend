import * as _ from 'lodash';

export class EmpresaModel {
  _id: string;
  nome: string;
  usuarios: any[];
  principal: boolean;
  parametrosReinf: EmpresaParametrosReinfModel;

  getModel () {
    return _.pick(['_id', 'nome', 'principal']);
  }
}

export class ParamReinfInformacaoContribuinte {
  classificacaoTributaria: string;
  indECDObrigatoria: boolean;
  indDesoneracao: boolean;
  indAcordoIsencaoMulta: boolean;
  situacaoPessoaJuridica: number;
}

export class ParamReinfContato {
  nomeContato: string;
  cpfContato: string;
  foneFixo: string;
  foneCelular: string;
  email: string;
}

export class EmpresaParametrosReinfModel {
  _id: string;
  empresa: string;
  informacaoContribuinte: ParamReinfInformacaoContribuinte;
  contato: ParamReinfContato;

  clear(empresaId: string) {
    this.empresa = empresaId;

    this.informacaoContribuinte = new ParamReinfInformacaoContribuinte();
    this.informacaoContribuinte.classificacaoTributaria = '';
    this.informacaoContribuinte.indECDObrigatoria = false;
    this.informacaoContribuinte.indDesoneracao = false;
    this.informacaoContribuinte.indAcordoIsencaoMulta = false;
    this.informacaoContribuinte.situacaoPessoaJuridica = null;

    this.contato = new ParamReinfContato();
    this.contato.nomeContato = '';
    this.contato.cpfContato = '';
  }
}
