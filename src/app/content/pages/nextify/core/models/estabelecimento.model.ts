import { EmpresaModel } from './empresa.model';

export class EstabelecimentoModel {
  _id: string;
  cnpj: string;
  nomeEmpresarial: string;
  nomeFantasia: string;
  principal: boolean;
}
