import { Injectable } from '@angular/core';
import { formatNumber } from '@angular/common';

@Injectable()
export class NumberUtilsService {
  numberFormat(value: number): string {
    if (!value) {
      return undefined;
    }
    // return formatNumber(value, 'pt', '1.2-5');
    return new Intl.NumberFormat('pt-BR', { useGrouping: false, minimumFractionDigits: 2}).format(value);
  }

  decimalParse(value: string): number {
    if (!value) {
      return undefined;
    }
    return parseFloat(value.toString().replace(',', '.'));
  }
}

