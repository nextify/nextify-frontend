import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { EmpresaModel, EmpresaParametrosReinfModel } from '../models/empresa.model';
import { HttpUtilsService } from '../../../../../core/services/http-utils.service';
import { ApiConfig } from '../../../../../config/api';

const API_URL = `${ApiConfig.url}/empresa`;
const API_URL_PARAM = `${ApiConfig.url}/empresa/param/reinf`;

@Injectable()
export class EmpresaService {
  constructor(private http: HttpClient, private httpUtils: HttpUtilsService) {}

  create(empresa): Observable<EmpresaModel> {
    return this.http.post<EmpresaModel>(API_URL, empresa);
  }

  update(empresa: EmpresaModel): Observable<EmpresaModel> {
    const url = API_URL;
    const _empresa = new EmpresaModel();
    Object.assign(_empresa, empresa);

    const httpHeaders = this.httpUtils.getHTTPHeaders();
    return this.http.put<EmpresaModel>(url, _empresa.getModel(), {
      headers: httpHeaders
    });
  }

  get(): Observable<EmpresaModel> {
    return this.http.get<EmpresaModel>(API_URL + `/`);
  }

// Parametros Reinf
  createParamReinf(param: EmpresaParametrosReinfModel): Observable<EmpresaParametrosReinfModel> {
    const url = API_URL_PARAM;
    return this.http.post<EmpresaParametrosReinfModel>(url, param);
  }

  updateParamReinf(param: EmpresaParametrosReinfModel): Observable<EmpresaParametrosReinfModel> {
    const url = `${API_URL_PARAM}/${param._id}`;
    const httpHeaders = this.httpUtils.getHTTPHeaders();
    return this.http.put<EmpresaParametrosReinfModel>(url, param, {
      headers: httpHeaders
    });
  }

  // getAll(): Observable<PessoaModel[]> {
  //   return this.http.get<PessoaModel[]>(API_URL);
  // }

  // getById(_id: string): Observable<PessoaModel> {
  //   return this.http.get<PessoaModel>(API_URL + `/${_id}`);
  // }

  // find(queryParams: QueryParamsModel): Observable<any[]> {
  //   const httpHeaders = this.httpUtils.getHTTPHeaders();
  //   const httpParams = this.httpUtils.getFindHTTPParams(queryParams);

  //   const url = API_PESSOA_URL + '/find';
  //   return this.http.get<PessoaModel[]>(url, {
  //     headers: httpHeaders,
  //     params: httpParams
  //   });
  // }
}
