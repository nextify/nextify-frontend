import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { EstabelecimentoModel } from '../models/estabelecimento.model';
import { HttpUtilsService } from '../../../../../core/services/http-utils.service';
import { ApiConfig } from '../../../../../config/api';

const API_URL = `${ApiConfig.url}/estabelecimento`;


@Injectable()
export class EstabelecimentoService {
  constructor(private http: HttpClient, private httpUtils: HttpUtilsService) {}

  create(empresa): Observable<EstabelecimentoModel> {
    return this.http.post<EstabelecimentoModel>(API_URL, empresa);
  }

  update(estabelecimento: EstabelecimentoModel): Observable<EstabelecimentoModel> {
    const url = API_URL;
    const httpHeaders = this.httpUtils.getHTTPHeaders();
    return this.http.put<EstabelecimentoModel>(url, estabelecimento, {
      headers: httpHeaders
    });
  }

  get(): Observable<EstabelecimentoModel> {
    return this.http.get<EstabelecimentoModel>(API_URL + `/`);
  }

  // getAll(): Observable<PessoaModel[]> {
  //   return this.http.get<PessoaModel[]>(API_URL);
  // }

  // getById(_id: string): Observable<PessoaModel> {
  //   return this.http.get<PessoaModel>(API_URL + `/${_id}`);
  // }

  // find(queryParams: QueryParamsModel): Observable<any[]> {
  //   const httpHeaders = this.httpUtils.getHTTPHeaders();
  //   const httpParams = this.httpUtils.getFindHTTPParams(queryParams);

  //   const url = API_PESSOA_URL + '/find';
  //   return this.http.get<PessoaModel[]>(url, {
  //     headers: httpHeaders,
  //     params: httpParams
  //   });
  // }
}
