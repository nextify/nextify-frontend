import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { PessoaModel } from '../models/pessoa.model';
import { QueryParamsModel } from '../../../../../core/models/query-params.model';
import { QueryResultsModel } from '../../../../../core/models/query-results.model';
import { HttpUtilsService } from '../../../../../core/services/http-utils.service';

const API_PESSOA_URL = 'http://localhost:3000/v1/pessoa';


@Injectable()
export class PessoaService {
  constructor(private http: HttpClient, private httpUtils: HttpUtilsService) {}

  create(pessoa): Observable<PessoaModel> {
    return this.http.post<PessoaModel>(API_PESSOA_URL, pessoa);
  }

  update(pessoa: PessoaModel): Observable<PessoaModel> {
    const url = API_PESSOA_URL + `/${pessoa._id}`;
    const httpHeaders = this.httpUtils.getHTTPHeaders();
    return this.http.put<PessoaModel>(url, pessoa, {
      headers: httpHeaders
    });
  }

  getAll(): Observable<PessoaModel[]> {
    return this.http.get<PessoaModel[]>(API_PESSOA_URL);
  }

  getById(_id: string): Observable<PessoaModel> {
    return this.http.get<PessoaModel>(API_PESSOA_URL + `/${_id}`);
  }

  find(queryParams: QueryParamsModel): Observable<any[]> {
    const httpHeaders = this.httpUtils.getHTTPHeaders();
    const httpParams = this.httpUtils.getFindHTTPParams(queryParams);

    const url = API_PESSOA_URL + '/find';
    return this.http.get<PessoaModel[]>(url, {
      headers: httpHeaders,
      params: httpParams
    });
  }
}
