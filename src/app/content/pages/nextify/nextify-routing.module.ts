import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NextifyComponent } from './nextify.component';
import { AuthGuard } from '../../../core/auth/auth.guard';

const routes: Routes = [
  {
    path: '',
    component: NextifyComponent,
    canLoad: [AuthGuard],
  },
  {
    path: 'basic',
    loadChildren: './basic/nextify-basic.module#NextifyBasicModule'
  },
  {
    path: 'reinf',
    loadChildren: './reinf/reinf.module#ReinfModule',
    canLoad: [AuthGuard]
  },
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule]
})
export class NextifyRoutingModule {}
