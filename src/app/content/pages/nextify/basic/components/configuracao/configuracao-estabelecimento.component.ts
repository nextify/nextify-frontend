import {Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import { BehaviorSubject, forkJoin } from 'rxjs';

import { LayoutUtilsService, MessageType } from '../../../../../../core/services/layout-utils.service';
import { EmpresaModel, EmpresaParametrosReinfModel } from '../../../core/models/empresa.model';
import { EmpresaService } from '../../../core/services/empresa.service';
import { EstabelecimentoService } from '../../../core/services/estabelecimento.service';
import { EstabelecimentoModel } from '../../../core/models/estabelecimento.model';
import { Router } from '@angular/router';

@Component({
	selector: 'm-config-estabelecimento',
	templateUrl: './configuracao-estabelecimento.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ConfiguracaoEstabelecimentoComponent implements OnInit {
  isLinear = true;
  estabelecimentoForm: FormGroup;
  paramEmpresaForm: FormGroup;
  contatoEmpresaForm: FormGroup;

  _empresa: EmpresaModel;
  _estabelecimento: EstabelecimentoModel;
  _paramEmpresa: EmpresaParametrosReinfModel;

  loadingSubject = new BehaviorSubject<boolean>(false);
  loading$ = this.loadingSubject.asObservable();

  classificacaoTributariaList: any[] = [
    {value: '01', name: '01 - Empresa enquadrada no regime de tributação Simples Nacional com tributação previdenciária substituída'},
    {value: '02', name: '02 - Empresa enquadrada no regime de tributação Simples Nacional com tributação previdenciária não substituída'},
    {value: '03', name: '03 - Empresa enquadrada no regime de tributação Simples Nacional com tributação previdenciária substituída e não substituída'},
    {value: '04', name: '04 - MEI - Micro Empreendedor Individual'},
    {value: '06', name: '06 - Agroindústria'},
    {value: '07', name: '07 - Produtor Rural Pessoa Jurídica'},
    {value: '08', name: '08 - Consórcio Simplificado de Produtores Rurais'},
    {value: '09', name: '09 - Órgão Gestor de Mão de Obra'},
    {value: '10', name: '10 - Entidade Sindical a que se refere a Lei 12.023/2009'},
    {value: '11', name: '11 - Associação Desportiva que mantém Clube de Futebol Profissional'},
    // tslint:disable-next-line:max-line-length
    {value: '13', name: '13 - Banco, caixa econômica, sociedade de crédito, financiamento e investimento e demais empresas relacionadas no parágrafo 1º do art. 22 da Lei 8.212./91'},
    {value: '14', name: '14 - Sindicatos em geral, exceto aquele classificado no código [10]'},
    {value: '21', name: '21 - Pessoa Física, exceto Segurado Especial'},
    {value: '22', name: '22 - Segurado Especial'},
    {value: '60', name: '60 - Missão Diplomática ou Repartição Consular de carreira estrangeira'},
    {value: '70', name: '70 - Empresa de que trata o Decreto 5.436/2005'},
    {value: '80', name: '80 - Entidade Beneficente de Assistência Social isenta de contribuições sociais'},
    {value: '85', name: '85 - Ente Federativo, Órgãos da União, Autarquias e Fundações Públicas'},
    {value: '99', name: '99 - Pessoas Jurídicas em Geral'},
  ];

  situacaoPessoaJuridicaList: any[] = [
    {value: '0', name: 'Situação Normal'},
    {value: '1', name: 'Extinção'},
    {value: '2', name: 'Fusão'},
    {value: '3', name: 'Cisão'},
    {value: '4', name: 'Incorporação'},
  ];

	constructor(private formBuilder: FormBuilder,
              private router: Router,
              private layoutUtilsService: LayoutUtilsService,
              private empresaService: EmpresaService,
              private estabelecimentoService: EstabelecimentoService) {
	}

  ngOnInit() {
    this.loadingSubject.next(true);

    this.empresaService.get().subscribe(res => {
      this._empresa = res;
      this._paramEmpresa = res.parametrosReinf;
      this.initParamEstabelecimento();
    });

    this.estabelecimentoService.get().subscribe(res => {
      this._estabelecimento = res;
      this.initEstabelecimento();
    });
  }

  initEstabelecimento() {
    this.estabelecimentoForm = this.formBuilder.group({
      cnpj: [this._estabelecimento.cnpj, Validators.required],
      nomeEmpresarial: [this._estabelecimento.nomeEmpresarial, Validators.required],
      nomeFantasia: [this._estabelecimento.nomeFantasia]
    });
  }

  initParamEstabelecimento() {
    if (!this._paramEmpresa) {
      this._paramEmpresa = new EmpresaParametrosReinfModel();
      this._paramEmpresa.clear(this._empresa._id);
    }
    // tslint:disable-next-line:max-line-length
    const situacaoPessoaJuridica = (this._paramEmpresa.informacaoContribuinte.situacaoPessoaJuridica === null) ? null : this._paramEmpresa.informacaoContribuinte.situacaoPessoaJuridica.toString();

    this.paramEmpresaForm = this.formBuilder.group({
      classificacaoTributaria: [this._paramEmpresa.informacaoContribuinte.classificacaoTributaria, Validators.required],
      indECDObrigatoria: [this._paramEmpresa.informacaoContribuinte.indECDObrigatoria],
      indDesoneracao: [this._paramEmpresa.informacaoContribuinte.indDesoneracao],
      indAcordoIsencaoMulta: [this._paramEmpresa.informacaoContribuinte.indAcordoIsencaoMulta],
      situacaoPessoaJuridica: [situacaoPessoaJuridica, Validators.required]
    });

    this.contatoEmpresaForm = this.formBuilder.group({
      nomeContato: [this._paramEmpresa.contato.nomeContato, Validators.required],
      cpfContato: [this._paramEmpresa.contato.cpfContato, Validators.required],
      foneFixo: [this._paramEmpresa.contato.foneFixo],
      foneCelular: [this._paramEmpresa.contato.foneCelular],
      email: [this._paramEmpresa.contato.email, Validators.email]
    });

    this.loadingSubject.next(false);
  }

  prepare() {
    const controls = this.estabelecimentoForm.controls;
    const paramControls = this.paramEmpresaForm.controls;
    const contatoControls = this.contatoEmpresaForm.controls;

    // Empresa
    this._empresa.nome = controls['nomeEmpresarial'].value;

    // Estabelecimento
    this._estabelecimento.nomeEmpresarial = controls['nomeEmpresarial'].value;
    this._estabelecimento.nomeFantasia = controls['nomeFantasia'].value;
    this._estabelecimento.cnpj = controls['cnpj'].value;

    // Parametros
    this._paramEmpresa.informacaoContribuinte.classificacaoTributaria = paramControls['classificacaoTributaria'].value;
    this._paramEmpresa.informacaoContribuinte.situacaoPessoaJuridica = +paramControls['situacaoPessoaJuridica'].value;
    this._paramEmpresa.informacaoContribuinte.indAcordoIsencaoMulta = paramControls['indAcordoIsencaoMulta'].value;
    this._paramEmpresa.informacaoContribuinte.indDesoneracao = paramControls['indDesoneracao'].value;
    this._paramEmpresa.informacaoContribuinte.indECDObrigatoria = paramControls['indECDObrigatoria'].value;

    // Contato
    this._paramEmpresa.contato.nomeContato = contatoControls['nomeContato'].value;
    this._paramEmpresa.contato.cpfContato = contatoControls['cpfContato'].value;
    this._paramEmpresa.contato.foneFixo = contatoControls['foneFixo'].value;
    this._paramEmpresa.contato.foneCelular = contatoControls['foneCelular'].value;
    this._paramEmpresa.contato.email = contatoControls['email'].value;
  }

  onSubmit() {
    this.prepare();
    this.update();
  }

  update() {
    this.loadingSubject.next(true);
    const tasks$: any[] = [this.empresaService.update(this._empresa)];

    tasks$.push(this.estabelecimentoService.update(this._estabelecimento));

    if (this._paramEmpresa._id) {
      tasks$.push(this.empresaService.updateParamReinf(this._paramEmpresa));
    } else {
      tasks$.push(this.empresaService.createParamReinf(this._paramEmpresa));
    }

    forkJoin(tasks$).subscribe(res => {
      this.loadingSubject.next(false);
      const message = `Os dados do Estabelecimento foram atualizados com sucesso.`;
      this.layoutUtilsService.showActionNotification(
        message,
        MessageType.Update,
        10000,
        true,
        false
      );
      setTimeout(() => {
        const _refreshUrl = '/';
        this.router.navigateByUrl(_refreshUrl);
      }, 300);
    }, response => {
      this.loadingSubject.next(false);
      console.log(response);
      this.layoutUtilsService.showActionNotification(
        response.error.message,
        MessageType.Update,
        10000,
        true,
        false
      );
    });

  }
}
