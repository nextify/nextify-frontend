import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

// import { ReinfNotafiscalListComponent } from './notafiscal-list/notafiscal-list.component';
// import { ReinfNotaFiscalEditComponent } from './notafiscal-edit/notafiscal-edit.component';
import { NextifySharedModule } from '../nextify-shared.module';
import { ConfiguracaoEstabelecimentoComponent } from './components/configuracao/configuracao-estabelecimento.component';
import { EmpresaService } from '../core/services/empresa.service';
import { HttpUtilsService } from '../../../../core/services/http-utils.service';
import { EstabelecimentoService } from '../core/services/estabelecimento.service';

// import { TwoDigitDecimaNumberDirective } from '../../../../../core/directives/two-digit-input.directive';
// import { ReinfNotaFiscalService } from '../core/services/notafiscal.service';
// import { ReinfTipoServicoListComponent } from './subs/tiposervico-list/tiposervico-list.component';
// import { ReinfTipoServicoEditComponent } from './subs/tiposervico-edit/tiposervico-edit.component';
// import { HttpUtilsService } from '../../../../../core/services/http-utils.service';
// import { NumberUtilsService } from '../../core/services/number-utils.service';

const routes: Routes = [
  {
    path: 'config',
    component: ConfiguracaoEstabelecimentoComponent
  }
];

@NgModule({
  imports: [
    FormsModule,
    ReactiveFormsModule,
    NextifySharedModule,
    RouterModule.forChild(routes)
  ],
  declarations: [
    ConfiguracaoEstabelecimentoComponent
  ],
  providers: [
    HttpUtilsService,
    EmpresaService,
    EstabelecimentoService
  ]
})
export class NextifyBasicModule {}
