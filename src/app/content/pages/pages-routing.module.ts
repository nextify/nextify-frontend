import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PagesComponent } from './pages.component';
import { ErrorPageComponent } from './snippets/error-page/error-page.component';
import { AuthGuard } from '../../core/auth/auth.guard';

const routes: Routes = [
	{
		path: '',
    canActivate: [AuthGuard],
    component: PagesComponent,
		loadChildren: './nextify/nextify.module#NextifyModule',
  },
  {
    path: 'login',
    loadChildren: './auth/auth.module#AuthModule',
  },
  {
		path: '404',
		component: ErrorPageComponent
	},
	{
		path: 'error/:type',
		component: ErrorPageComponent
	}
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule]
})
export class PagesRoutingModule {
}
