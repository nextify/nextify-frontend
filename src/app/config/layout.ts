import { ConfigModel } from '../core/interfaces/config';

export class LayoutConfig implements ConfigModel {
  public config: any = {
    "demo": "demo2",
    "self": {
      "layout": "fluid",
      "background": ""
    },
    "loader": {
      "enabled": true,
      "image": "./assets/demo/demo2/media/img/logo/logo_large.png"
    },
    "header": {
      "self": {
        "fixed": {
          "desktop": true,
          "mobile": true,
          "minimize": {
            "desktop": {
              "enabled": true,
              "offset": 201
            },
            "mobile": {
              "enabled": false,
              "offset": 200
            }
          }
        },
        "logo": "./assets/demo/demo2/media/img/logo/logo.png"
      },
      "search": {
        "type": "search-default",
        "dropdown": {
          "skin": "light"
        }
      }
    },
    "aside": {
      "left": {
        "display": true,
        "fixed": true,
        "skin": "light",
        "push_footer": true,
        "minimize": {
          "toggle": false,
          "default": false
        }
      },
      "right": {
        "display": true
      }
    },
    "menu": {
      "header": {
        "display": true,
        "desktop": {
          "skin": "dark",
          "arrow": true,
          "toggle": "click",
          "submenu": {
            "skin": "light",
            "arrow": true
          }
        },
        "mobile": {
          "skin": "dark"
        }
      },
      "aside": {
        "display": true,
        "desktop_and_mobile": {
          "submenu": {
            "skin": "inherit",
            "accordion": true,
            "dropdown": {
              "arrow": true,
              "hover_timeout": 500
            }
          },
          "minimize": {
            "submenu_type": "default"
          }
        }
      }
    },
    "content": {
      "skin": "light2"
    },
    "footer": {
      "fixed": true
    },
    "quicksidebar": {
      "display": true
    },
    "portlet": {
      "sticky": {
        "offset": 50
      }
    }
  };

	constructor(config?: any) {
		if (config) {
			this.config = config;
		}
	}
}
